[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/shoppinglist.koenvervloesem)

# Shopping List

This is the example application for the [Ubuntu Touch app development training course](https://ubports.gitlab.io/marketing/education/ub-clickable-1/index.html). This course teaches you how to create this app.

![Screenshot](screenshot.png)

## License

Copyright (C) 2022 Koen Vervloesem

Based on input and [code](https://github.com/codefounders-nl/ubtouch-shoppinglist) by Felix van Loenen, Terence Sambo, Leandro d'Agostino and Sander Klootwijk.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
